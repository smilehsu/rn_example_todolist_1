import React, { useState } from 'react';
import { StyleSheet, Text, View, KeyboardAvoidingView, Platform, TextInput, TouchableOpacity, Keyboard } from 'react-native';

import Task from './src/components/Task';


export default function App() {
  const [task, setTask] = useState("")
  const [taskItems, setItemsTask] = useState([])


  const handleAddTask = () => {
    // console.log(task)
    setItemsTask([...taskItems, task])
    //清空輸入框
    setTask(null);
    //關掉 手機鍵盤
    Keyboard.dismiss();
  }

  const completeTask = (index) => {
    let itemCopy = [...taskItems]
    itemCopy.splice(index, 1)

    setItemsTask(itemCopy)

  }


  return (
    <View style={styles.container}>
      <View style={styles.tasksWrapper}>
        <Text style={styles.sectionTitle}>今天的待辦事項</Text>
        <View style={styles.items} >
          {
            taskItems.map((item, index) => {
              return (
                <TouchableOpacity key={index} onPress={() => completeTask(index)}>
                  < Task title={item} />
                </TouchableOpacity>
              )

            })
          }


          {/* <Task title={"待辦事項 一"} />
          <Task title={"待辦事項 二"} />
          <Task title={"待辦事項 三"} /> */}
        </View>
      </View>

      {/* 待辦事項輸入區 */}
      <KeyboardAvoidingView
        behavior={Platform.OS === "android" ? "padding" : "height"}
        style={styles.writeTaskWrapper}
      >

        <TextInput
          placeholder='今天要做什麼?'
          style={styles.input}
          onChangeText={(text) => setTask(text)}
          value={task}
        />

        <TouchableOpacity onPress={() => handleAddTask()}>
          <View style={styles.addWrapper}>
            <Text style={styles.addText}>+</Text>
          </View>
        </TouchableOpacity>

      </KeyboardAvoidingView>


    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e8eaed',
    // justifyContent: "center"

  },
  tasksWrapper: {
    paddingTop: 60,
    paddingHorizontal: 20,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "bold",
  },
  items: {
    marginTop: 30,
  },
  // 下方輸入的區塊 (全部)
  writeTaskWrapper: {
    position: "absolute",
    bottom: 60,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  //左邊的input 區塊
  input: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: "#fff",
    width: 250,
    borderRadius: 60,
    borderColor: "#c0c0c0",
    borderWidth: 1,

  },
  //右邊的 +號 區塊
  addWrapper: {
    width: 60,
    height: 60,
    backgroundColor: "#fff",
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#c0c0c0",
    borderWidth: 1,
  },
  addText: {},


});
